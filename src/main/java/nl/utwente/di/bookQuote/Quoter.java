package nl.utwente.di.bookQuote;

public class Quoter {
    public Double getFahrenheit(String temp){
        Double fahrenheit;
        Double celsius = Double.valueOf(temp);
        fahrenheit = celsius * 9 / 5 + 32;
        return fahrenheit;
    }
}
